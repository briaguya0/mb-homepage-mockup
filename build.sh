rm -rf ./public
mkdir public
mkdir public/img
cp src/mockup.html public/index.html
cp -r src/img/responsive public/img
cp src/img/logo.svg public/img
cp src/img/logo-and-text.svg public/img
cp src/img/person-inverted.svg public/img
cp src/img/down-arrow.svg public/img
cp src/img/plus.svg public/img
cp src/img/search.svg public/img
sassc src/mockup.scss public/mockup.css
